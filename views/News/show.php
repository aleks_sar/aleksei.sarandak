<?php

$this->registerCssFile("@web/css/news.css", [
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
]);

$this->registerJsFile(
    '@web/js/button.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

echo $this->context->Calendar();

?>

<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css%22%3E
</head>

<body class="text-center">

    <h1><?php echo $heading; ?></h1>
    <p class="lead"><?php echo $body; ?></p>
    <button type="button" class="btn btn-danger" onclick="myFunction()">Try it</button>

    </body>