<?php
namespace app\controllers;

use yii\web\Controller;

class NewsController extends Controller
{
    public function actionShow()
    {
        $this->view->title = "News";

        $heading = "Pavel Durov is a Real Pavel Durov!";
        $body = "That means Pavel Durov is a Real Pavel Durov! BELIEVE IN THAT!";


        return $this->render('show',
            [
                'heading' => $heading,
                'body' => $body,

            ]);

    }


    public function Calendar()
    {
        echo '<label class="control-label">Calendar as bonus task!</label>';
        echo DatePicker::widget([
            'name' => 'dp_2',
            'type' => DatePicker::TYPE_COMPONENT_PREPEND,
            'value' => '23-Feb-1982',
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'dd-M-yyyy'
            ]
        ]);
    }



}


